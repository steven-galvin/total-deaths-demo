import csv

# Intitialize each years total deaths at 0

deaths2018 = 0
deaths2019 = 0
deaths2020 = 0

# Open CSV file

with open('NCHSData48.csv', 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        
        # Select each week from 1 - 48 of 2018 and add deaths from that week to yearly total

        # row[0] points to the "Year" column, row[1] points to the "Week" column, row[6] points to the "All Deaths" column

        if ((row[0] == '2018') and (int(row[1]) < 49)):
            deaths2018 += int(row[6])
        
        # Select each week from 1 - 48 of 2019 and add deaths from that week to yearly total

        if ((row[0] == '2019') and (int(row[1]) < 49)):
            deaths2019 += int(row[6])

        # Select each week from 1 - 48 of 2020 and add deaths from that week to yearly total

        if ((row[0] == '2020') and (int(row[1]) < 49)):
            deaths2020 += int(row[6])

    # Print yearly totals and differences
    
    print("2018 Deaths: " + str(deaths2018))
    print("2019 Deaths: " + str(deaths2019))
    print("2020 Deaths: " + str(deaths2020))

    print("There are " + (str(deaths2020 - deaths2019)) + " more deaths by week 48 of 2020 than week 48 of 2019")
    print("There are " + (str(deaths2020 - deaths2018)) + " more deaths by week 48 of 2020 than week 48 of 2018")